Source: libnet-async-tangence-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Andrej Shadura <andrewsh@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               perl
Build-Depends-Indep: libfuture-perl <!nocheck>,
                     libio-async-perl <!nocheck>,
                     libtangence-perl (>= 0.25) <!nocheck>,
                     libtest-fatal-perl <!nocheck>,
                     libtest-hexstring-perl <!nocheck>,
                     libtest-identity-perl <!nocheck>,
                     libtest-memory-cycle-perl <!nocheck>,
                     libtest-refcount-perl <!nocheck>,
                     liburi-perl <!nocheck>
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libnet-async-tangence-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libnet-async-tangence-perl.git
Homepage: https://metacpan.org/release/Net-Async-Tangence
Rules-Requires-Root: no

Package: libnet-async-tangence-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libfuture-perl,
         libio-async-perl,
         libtangence-perl (>= 0.25),
         liburi-perl
Description: module to use Tangence with IO::Async
 This distribution provides concrete implementations of the Tangence base
 classes, allowing either servers or clients to be written based on IO::Async.
 .
 To implement a server, see Net::Async::Tangence::Server.
 .
 To implement a client, see Net::Async::Tangence::Client.
 .
 Net::Async::Tangence itself does not provide any code, and exists only to
 provide the module $VERSION and top-level documentation.
